//getting Angular SCOPE
var angularScope;
window.onload = function(){
	angularScope = angular.element(document.getElementById('main')).scope();
};

var gridApp = {
	settings : {
		dx: 180, dy: 120,
		colors: {blue: '#2fa3d6'}
	}
};
var DayID = 0;
var PointID = 0;
var PointIDMax = 0;
var TransferID = 0;
var TransferIDMax = 0;
var ActivityID = 0;
var ActivityIDMax = 0;
var NoteID = 0;
var NoteIDMax = 0;
var AccomodationID = 0;
var AccomodationIDMax = 0;
/////// FUNCTIONS
gridApp.getClosest = function(point,points){
	var closest = points[0];
	for(var i = 0; i < points.length; i++){
		if(
			(point.x - points[i].x)*(point.x - points[i].x) + (point.y - points[i].y)*(point.y - points[i].y) 
			< 
			(point.x - closest.x)*(point.x - closest.x) + (point.y - closest.y)*(point.y - closest.y)
		){
			closest = points[i];
		}
	}
	return closest;
};


////////////////////////////////////////////////////////////

Snap.plugin(function(Snap, Element, Paper, glob){

	/********************************************/
	/***********   GENERAL METHODS   ************/
	/********************************************/

	//get country by Y-coordinate
	Paper.prototype.getCountry = function(y){
		var countries = gridApp.gCountries.selectAll('.country');
		for(var i = 0; i < countries.length; i++){
			if(y > countries[i].getBBox().y && y < (countries[i].getBBox().y + countries[i].getBBox().height)){
				return countries[i].data('country');
				break;
			}
		};
		return false;
	};

	/*********** ELEMENT METHODS ***************/

	Element.prototype.move = function(dx,dy){
		var x = this.data('origPosition').x + dx;
		var y = this.data('origPosition').y + dy;
		this.moveXY(x,y);
	};

	Element.prototype.moveXY = function(x,y){
		var matrix = new Snap.Matrix();
		matrix.translate(x,y);
		this.transform(matrix);
	};

	Element.prototype.animateXY = function(x,y,callback){
		var matrix = new Snap.Matrix();
		matrix.translate(x, y); 
		this.animate({ transform: matrix }, 200, mina.easein, function(){
			if(callback){
				callback();
			}
		});
	};

	/********************************************/
	/*************   GRID CLASS   ***************/
	/********************************************/
	Paper.prototype.Day = function(dayIndex){
		
		var el = this.group();
		el.settings = {
			days : [
				{id: 0, title: 'mon'},
				{id: 1, title: 'tue'},
				{id: 2, title: 'wed'},
				{id: 3, title: 'thu'},
				{id: 4, title: 'fri'},
				{id: 5, title: 'sat'},
				{id: 6, title: 'sun'}
			]
		};
		if(dayIndex){
			DayID = dayIndex;
		}else{
			if(dayIndex === 0){
				DayID = dayIndex;
			}else{
				dayIndex = DayID%(el.settings.days.length);
			}
		}
		DayID++;
		day = el.settings.days[dayIndex];

		el.idx = day.id;
		el.attr({
			id: 'day-' + DayID,
			class: 'day',
		}).data({day: day});

		var left = canvas.line(0, 0, 0, '100%').attr({stroke: gridApp.settings.colors.blue ,strokeWidth: 1,strokeDasharray: '1,5'});
		el.add(left);

		var right = canvas.line(gridApp.settings.dx+1, 0, gridApp.settings.dx+1, '100%').attr({stroke: 'transparent', strokeWidth: 1});
		el.add(right);

		var x = gridApp.settings.dx + gridApp.gDays.selectAll('.day').length * gridApp.settings.dx;
		el.moveXY(x,0);

		//resize the with of canvas
		canvas.attr('style','width:'+x+'px');

		var label = canvas.text(5, gridApp.settings.dy-5, day.title).attr({'text-anchor': 'start', 'font-size': '0.75rem', 'fill': '#2fa3d6'});
		el.add(label);

		gridApp.gDays.add(el);
		return el;
	};

	Paper.prototype.Country = function(country){
		if(!country) return false;

		var el = this.group();
		el.attr({
			id: 'country-' + country.id,
			class: 'country',
		}).data({country: country});

		var top = canvas.line(0, 0, '100%', 0).attr({stroke: 'transparent', strokeWidth: 1});
		el.add(top);

		var bottom = canvas.line(0, gridApp.settings.dy+1, '100%', gridApp.settings.dy+1).attr({stroke: gridApp.settings.colors.blue ,strokeWidth: 1,strokeDasharray: '1,5'});
		el.add(bottom);

		var label = canvas.text(gridApp.settings.dx-5, gridApp.settings.dy-5, country.title).attr({'text-anchor': 'end', 'font-size': '0.75rem', 'fill': '#2fa3d6'});
		el.add(label);

		var y = gridApp.gCountries.selectAll('.country').length * gridApp.settings.dy;
		el.moveXY(0,y);

		el.cons = function(){
			console.log(el);
		};

		gridApp.gCountries.add(el);
		return el;
	};

	/********************************************/
	/**********   CONNECTION CLASS   ************/
	/********************************************/
	Paper.prototype.Connection = function(points){
		if(!points){
			console.log('Not enough parameters for connection object. Example: {from: "point_obj", to:"point_obj" }');
			return false;
		}

		var el = this.group();
		el.points = points;
		el.attr({id: 'connection-'+points.from.idx+'-'+points.to.idx, class: 'connection'});
		el.line = canvas.line(
			points.from.getBase().x,
			points.from.getBase().y,
			points.to.getBase().x,
			points.to.getBase().y
		)
		.attr({fill: 'none', stroke: gridApp.settings.colors.blue});
		el.add(el.line);
		el.transfers = {};

		//assign this connection to points
		el.points.from.connections.right = el;
		el.points.to.connections.left = el;

		el.hide = function(){
			el.attr('visibility','hidden');
		};
		el.show = function(){
			el.attr('visibility','visible');
		};

		el.update = function(){
			var that = this;
			that.line.attr({
				x1: that.points.from.getBase().x, 
				y1: that.points.from.getBase().y,
				x2: that.points.to.getBase().x, 
				y2: that.points.to.getBase().y
			});
			that.show();
		};

		el.delete = function(){
			el.points.from.connections.right = null;
			el.points.to.connections.left = null;

			el.line.remove();
			el.remove();
		};
		gridApp.gConnections.add(el);
		return el;
	};

	/********************************************/
	/************   MARKER CLASS   **************/
	/********************************************/
	Paper.prototype.Point = function(point){
		var paper = this;
		if(point){
			PointID = point.id;
			PointIDMax = point.id > PointIDMax ? point.id : PointIDMax;
			PointID = PointIDMax;

			//create country if not exist
			if(point.place.country.id){
				var country = gridApp.gCountries.select('#country-'+point.place.country.id);
				if(!country){
					canvas.Country(point.place.country);
				}
			}

		}else{
			PointID++;
		}

		var el = paper.group();

		el.settings = {
			base : {x: 50, y: 75},
			center : {x: 50, y: 40},
			label : {x: 0, y: 16}
		};

		el.idx = PointID;
		el.attr({
			'id': 'point-'+PointID, 
			'class': 'point', 
			'transform': 'matrix(1,0,0,1,20,20)'
		}).data({
			'origPosition': {x: 0, y: 0},
			'prevPosition': {x: 0, y: 0},
			'place': {}
		});
		el.connections = {left: null, right: null};

		Snap.load('img/city.svg',function(file){
			el.node.innerHTML = file.node.innerHTML;
			el.gElements = {
				activities: el.select('.elements .activities'), 
				accomodations: el.select('.elements .accomodations'),
				notes: el.select('.elements .notes')
			};
			//initial data
			if(point){
				el.select('.label').node.innerHTML = point.place.city.title;
				el.data({'place': point.place});
				el.moveXY(el.getX(point.time), 0);
				el.align();
			}
			el.select('.accomodations .plus').click(function(){
				canvas.Accomodation(null,el);
			});
			el.select('.activities .plus').click(function(){
				canvas.Activity(null,el);
			});
			el.select('.notes .plus').click(function(){
				canvas.Note(null,el);
			});
		});

		el.click(function(event){
			event.stopPropagation();
		});

		el.mousedown(function(event){
			event.stopPropagation();
			if (event.which == 3){
				$('#point-menu').show().css({ top: event.clientY, left: event.clientX}).attr('data-id',el.attr('id'));
			}
		});

		el.getBase = function(){
			var that = this;
			return {
				x: that.getBBox().x + that.settings.base.x,
				y: that.getBBox().y + that.settings.base.y
			};
		};

		el.getCenter = function(){
			var that = this;
			return {
				x: that.getBBox().x + that.settings.center.x,
				y: that.getBBox().y + that.settings.center.y
			};
		};

		el.getX = function(seconds){
			var that = this;
			return parseInt(seconds*gridApp.settings.dx/(24*3600)) + gridApp.settings.dx - that.settings.center.x;
		};

		el.getTime = function(){
			var that = this;
			var time = {from:'',to:''};
			time['from'] = (that.getBBox().x+that.settings.center.x)*24*3600/gridApp.settings.dx - 24*3600;
			time['to'] = time['from'] + 24 * 3600;
			return time;
		};

		el.draggable = function(){
			var that = this;
			var moved = false;
			that.drag(
				function(dx,dy){
					var x = that.data('origPosition').x + dx;
					var y = that.data('origPosition').y + dy + that.settings.label.y;
					if(Math.abs(dx) > 0 || Math.abs(dy) > 0){
						moved = true;	
					}
					that.moveXY(x,y);
				},
				function(){
					that.data({'origPosition': that.getBBox()});
					if(that.connections.left){
						that.connections.left.hide();
					}
					if(that.connections.right){
						that.connections.right.hide();
					}
				},
				function(){
					if(moved){
						that.moved();
						moved = false;
					}else{
						that.align();
					}
				}
			);
		};

		//call this if point is moved or edited
		el.moved = function(callback){
			var that = this;
			var transfers = [];
			if(that.connections.left){
				var transfer = that.connections.left.select('.transfer');
				if(transfer) transfers.push(transfer);
			}
			if(that.connections.right){
				var transfer = that.connections.right.select('.transfer');
				if(transfer) transfers.push(transfer);
			}
			if(transfers.length > 0){
				if(confirm("Удалить связанные трансферы и переместить?")){
					if(that.connections.left){
						var transfer = that.connections.left.select('.transfer');
						if(transfer) transfer.delete();
						that.connections.left.hide();
					}
					if(that.connections.right){
						var transfer = that.connections.right.select('.transfer');
						if(transfer) transfer.delete();
						that.connections.right.hide();
					}
					if(callback) callback(true);
					that.align();
				}else{
					if(callback) callback(false);
					that.align(true);
				}
			}else{
				if(callback) callback(true);
				that.align();
			}
		};


		el.align = function(prev){
			var that = this;
			var x, y;

			var zeroPoint = gridApp.settings.dx - that.settings.center.x;
			var newPosition = {
				x: that.getBBox().x < zeroPoint ? zeroPoint : that.getBBox().x,
				y: gridApp.gCountries.select('#country-'+that.data('place').country.id).getBBox().cy - that.settings.center.y
			};

			if(prev){
				that.animateXY(that.data('prevPosition').x, that.data('prevPosition').y, function(){
					if(that.connections.left) that.connections.left.show();
					if(that.connections.right) that.connections.right.show();
				});
			}else{
				if(newPosition.x+gridApp.settings.dx > gridApp.gDays.getBBox().x2){
					canvas.Day();
					//console.log('new day');
				}
				that.animateXY(newPosition.x, newPosition.y, function(){
					that.data({'prevPosition':newPosition});
					that.connect();
					console.log('moved');
				});
			}
			
		};


		el.connect = function(){
			var that = this;		
			var x = that.getBase().x;
			var mPoints = gridApp.gPoints.selectAll('.point');

			if(mPoints.length < 2){
				return false;
			}			
			//sorting the array of selected points by time (x-axis)
			var points = []
			mPoints.forEach(function(item){
				points.push(item);
			});
			points.sort(function(a,b){return a.getBBox().x - b.getBBox().x;});

			var new_neighbors = false;
			for(var i = 0; i < points.length; i++){
				if(points[i] == that){
					//is left neighbors
					if(i > 0){
						if(that.connections.left){
							if(points[i-1].idx !== that.connections.left.points.from.idx){
								//new left neighbor
								if(points[i-1].connections.right) points[i-1].connections.right.delete();
								new_neighbors = true;
							}
						}else{
							//new left neighbor
							if(points[i-1].connections.right) points[i-1].connections.right.delete();
							new_neighbors = true;
						}

					}
					//is right neighbors
					if(i+1 < points.length){
						if(that.connections.right){
							if(points[i+1] !== that.connections.right.points.to){
								//new right neighbor
								if(points[i+1].connections.left) points[i+1].connections.left.delete();
								new_neighbors = true;
							}
						}else{
							//new right neighbor
							if(points[i+1].connections.left) points[i+1].connections.left.delete();
							new_neighbors = true;
						}
					}
				}
			}
			//no need to check if neighbors are new - tranfers anyway removed
			//if new neighbors - remove old connection for this
			if(new_neighbors){
				if(that.connections.left) that.connections.left.delete();
				if(that.connections.right) that.connections.right.delete();
			}	
			

			for(var i = 1; i < points.length; i++){
				//create new connections if they not exists or update if exists
				if(points[i].connections.left){
					points[i].connections.left.update();
				}else{
					canvas.Connection({from: points[i-1], to: points[i]});
				}
			}
		};

		//edit function
		el.update = function(){
			var that = this;
			var pointData = {id: that.idx, place: that.data('place'), time: that.getTime()};
			angularScope.updatePoint(pointData,function(point){
				if(point.success){
					//if new place - call "moved"
					if(that.data('place') !== point.place){
						that.moved(function(status){
							if(status){
								if(!gridApp.gCountries.select('#country-'+point.place.country.id)){
									canvas.Country(point.place.country);
								}
								that.data({place: point.place});
								that.select('.label').node.innerHTML = point.place.city.title;
							}
						});
					}
				}else{
					that.delete();
				}
			});
			angularScope.$apply();
		};

		el.delete = function(){
			var that = this;
			//remove connections
			var connections = gridApp.gConnections.selectAll('.connection');
			var neighbor_id = false;
			for(var i = 0; i < connections.length; i++){
				var nodes = connections[i].node.id.replace('connection-','').split('-');
				if(nodes[0] == that.idx || nodes[1] == that.idx){
					connections[i].remove();
					//neighbors
					if(nodes[0] !== that.idx){
						neighbor_id = nodes[0];
					}else if(nodes[1] !== that.idx){
						neighbor_id = nodes[1];
					}
				}
			}
			that.remove();
			if(neighbor_id){
				gridApp.gPoints.select('#point-'+neighbor_id).connect();
			}
		};

		el.synch = function(){
			var that = this;
			var data = {
				point: {id: that.idx, 
				time: that.getTime(), 
				country: that.data('country'), 
				city: that.data('city')}
			};
			$.ajax({
				url: 'server/point.php',
				type: 'POST',
				data: data
			}).done(function(response){
				//console.log(response);
				response = $.parseJSON(response);
				if(response.error){
					//console.log('error')
					//that.align(true);
					//return false;
				}
			});
		};

		gridApp.gPoints.add(el);
		return el;
	};

	/********************************************/
	/***********   TRANSFER CLASS   *************/
	/********************************************/
    Paper.prototype.Transfer = function(transfer){
    	if(transfer){
    		TransferID = transfer.id;
			TransferIDMax = transfer.id > TransferIDMax ? transfer.id : TransferIDMax;
			TransferID = TransferIDMax;
		}else{
			TransferID++;
		}

    	var el = this.group();
    	el.settings = {
    		center : {x: 17, y: 17}
    	};
    	el.idx = TransferID;
    	el.attr({
			'id': 'transfer-'+TransferID,
			'class': 'transfer',
			'transform': 'matrix(1,0,0,1,20,70)'
		}).data({
			'origPosition': el.getBBox(), 
			'form':'',
			'to':''
		});

		el.click(function(event){
			event.stopPropagation();
			//console.log(el);
		});

		el.mousedown(function(event){
			event.stopPropagation();
			if (event.which == 3){
				$('#transfer-menu').show().css({ top: event.clientY, left: event.clientX}).attr('data-id',el.attr('id'));
			}
		});

		el.setType = function(type){
			var that = this;
			var icon;
			switch(type){
				case "flight":
					icon = 'flight.svg';
					break;
				case "bus":
					icon = 'bus.svg';
					break;
				case "railway":
					icon = 'railway.svg';
					break;
				case "car":
					icon = 'car.svg';
					break;
				case "ship":
					icon = 'ship.svg';
					break;
				default:
					icon = 'flight.svg';
			}
			Snap.load('img/transfer/'+icon, function(file){
				that.append(file);
			});
			that.attr({type:type});
		};

		el.connect = function(){
			var that = this;
			//get closest points->points
			var x = that.getBBox().x + that.settings.center.x;
			var y = that.getBBox().y + that.settings.center.y;

			var points = [];
			gridApp.gConnections.selectAll('.connection').forEach(function(el){
				points.push({id:el.node.id, x: el.getBBox().cx, y: el.getBBox().cy})
			});
			if(points.length > 0){
				//closest connection
				var connection = gridApp.getClosest({x:x, y:y},points);
				var gConnnection = gridApp.gConnections.select('#'+connection.id);
				gConnnection.add(that);

				//get points
				var from = gConnnection.points.from;
				var to = gConnnection.points.to;

				that.data({
					'center': {x: connection.x, y: connection.y},
					'from': {id: from.idx, place: from.data('place'), time: from.getTime().to},
					'to': {id: to.idx, place: to.data('place'), time: to.getTime().from}
				});

				that.align();
			}else{
				that.remove();
			}
		};

		el.align = function(){
			var that = this;
			that.animateXY(that.data('center').x - that.settings.center.x, that.data('center').y - that.settings.center.y, function(){
				that.update();
			});
		};
		//передать статус
		el.update = function(){
			var that = this;
			angularScope.updateTransfer({id: that.idx, type: that.attr('type'), from: that.data('from'), to: that.data('to')}, function(result){
				if(result.success){
					var from = that.data('from');
					from.place = result.from.place;
					from.time = result.from.time;

					var to = that.data('to');
					to.place = result.to.place;
					to.time = result.to.time;

					that.data({from: from, to: to});

					that.setType(result.type);
					//that.synch();
				}else{
					that.remove();
				}
			});
			angularScope.$apply();
		};

		el.delete = function(){
			this.remove();
		};

		el.synch = function(){
			var that = this;
			var data = {
				transfer: {
					id: that.idx,
					from: that.data('from'),
					to: that.data('to')
				}
			};
			$.ajax({
				url: 'server/transfer.php',
				type: 'POST',
				data: data
			}).done(function(response){
				//console.log(response);
				response = $.parseJSON(response);
				if(response.error){
					console.log('error');
					//that.align(true);
					//return false;
				}
			});
		};

		return el;
    };

	/********************************************/
	/*********   ACCOMODATION CLASS   ***********/
	/********************************************/
    Paper.prototype.Accomodation = function(accomodation,point){
    	if(accomodation){
    		AccomodationID = accomodation.id;
			AccomodationIDMax = accomodation.id > AccomodationIDMax ? accomodation.id : AccomodationIDMax;
			AccomodationID = AccomodationIDMax;
		}else{
			AccomodationID++;
		}

		var el = this.group();

		el.settings = {
    		center : {x: 17, y: 17}
    	};

		el.idx = AccomodationID;
		el.attr({
			'id': 'accomodation-'+AccomodationID,
			'class': 'accomodation'
		}).data({
		});
		Snap.load('img/accomodation.svg', function(file){
			var svg = file.node.innerHTML;
			el.node.innerHTML = svg;
			if(point) el.addTo(point,true);
		});

		el.click(function(event){
			event.stopPropagation();
			console.log(el);
		});

		el.addTo = function(point,direct){
			var that = this;
			if(!point) return false;
			var offsetY = 0;
			var actualItems = point.selectAll('.accomodation');
			console.log(actualItems);
			actualItems.forEach(function(item){
				if(item.getBBox().y > offsetY){
					offsetY = item.getBBox().y;
				}
			});
			if(actualItems.length > 0) offsetY+=5;
			//if adding from context-menu
			if(direct){
				point.gElements.accomodations.add(that);
				that.moveXY(0,offsetY);
				that.update();
			}else{
				that.animateXY(
					point.getBBox().x + point.gElements.accomodations.getBBox().x,
					point.getBBox().y + point.gElements.accomodations.getBBox().y + offsetY + 5,
					function(){
						point.gElements.accomodations.add(that);
						that.moveXY(0,offsetY);
						that.update();
					}
				);
			}
		};

		el.connect = function(){
			var that = this;
			//get closest points
			var x = that.getBBox().x + that.settings.center.x;
			var y = that.getBBox().y + that.settings.center.y;

			var points = [];
			gridApp.gPoints.selectAll('.point').forEach(function(el){
				points.push({id:el.node.id, x: el.getCenter().x, y: el.getCenter().y});
			});

			if(points.length > 0){
				var point = gridApp.getClosest({x:x, y:y},points);
				var gPoint = gridApp.gPoints.select('#'+point.id);
				if(gPoint) that.addTo(gPoint);
			}
		};

		el.mousedown(function(event){
			event.stopPropagation();
			if (event.which == 3){
				$('#point-element-menu').show().css({ top: event.clientY, left: event.clientX}).attr('data-id',el.attr('id'));
			}
		});

		el.update = function(){
			var that = this;
			angularScope.updateAccomodation({id: that.idx}, function(result){
				console.log(result);
				if(result.success){	
					//that.synch();
				}else{
					that.remove();
				}
			});
			angularScope.$apply();
		}

		return el;

	};

	/********************************************/
	/***********   ACTIVITY CLASS   *************/
	/********************************************/
    Paper.prototype.Activity = function(activity,point){
    	if(!point){
    		//return false;
    	}
    	if(activity){
    		ActivityID = activity.id;
			ActivityIDMax = activity.id > ActivityIDMax ? activity.id : ActivityIDMax;
			ActivityID = ActivityIDMax;
		}else{
			ActivityID++;
		}

		var el = this.group();

		el.settings = {
    		center : {x: 17, y: 17}
    	};

		el.idx = ActivityID;
		el.attr({
			'id': 'activity-'+ActivityID,
			'class': 'activity'
		}).data({
		});

		Snap.load('img/activity.svg', function(file){
			var svg = file.node.innerHTML;
			el.node.innerHTML = svg;
			if(point) el.addTo(point,true);
		});

		el.click(function(event){
			event.stopPropagation();
			console.log(el);
		});

		el.addTo = function(point,direct){
			var that = this;
			if(!point) return false;
			var offsetY = 0;
			var actualItems = point.selectAll('.activity');
			actualItems.forEach(function(item){
				if(item.getBBox().y > offsetY){
					offsetY = item.getBBox().y;
				}
			});
			if(actualItems.length > 0) offsetY+=5;
			//if adding from context-menu
			if(direct){
				point.gElements.activities.add(that);
				that.moveXY(0,offsetY);
				that.update();
			}else{
				that.animateXY(
					point.getBBox().x + point.gElements.activities.getBBox().x,
					point.getBBox().y + point.gElements.activities.getBBox().y + offsetY + 5,
					function(){
						point.gElements.activities.add(that);
						that.moveXY(0,offsetY);
						that.update();
					}
				);
			}
		};

		el.connect = function(){
			var that = this;
			//get closest points
			var x = that.getBBox().x + that.settings.center.x;
			var y = that.getBBox().y + that.settings.center.y;

			var points = [];
			gridApp.gPoints.selectAll('.point').forEach(function(el){
				points.push({id:el.node.id, x: el.getCenter().x, y: el.getCenter().y});
			});

			if(points.length > 0){
				var point = gridApp.getClosest({x:x, y:y},points);
				var gPoint = gridApp.gPoints.select('#'+point.id);
				if(gPoint) that.addTo(gPoint);
			}
		};

		el.mousedown(function(event){
			event.stopPropagation();
			if (event.which == 3){
				$('#point-element-menu').show().css({ top: event.clientY, left: event.clientX}).attr('data-id',el.attr('id'));
			}
		});

		el.update = function(){
			var that = this;
			angularScope.updateActivity({id: that.idx}, function(result){
				if(result.success){	
					//that.synch();
				}else{
					that.remove();
				}
			});
			angularScope.$apply();
		}

		return el;

	};

	/********************************************/
	/*************   NOTE CLASS   ***************/
	/********************************************/
    Paper.prototype.Note = function(note,point){
    	if(!point){
    		//return false;
    	}
    	if(note){
    		NoteID = note.id;
			NoteIDMax = note.id > NoteIDMax ? note.id : NoteIDMax;
			NoteID = NoteIDMax;
		}else{
			NoteID++;
		}

		var el = this.group();

		el.settings = {
    		center : {x: 17, y: 17}
    	};

		el.idx = NoteID;
		el.attr({
			'id': 'note-'+NoteID,
			'class': 'note'
		}).data({
		});

		Snap.load('img/note.svg', function(file){
			var svg = file.node.innerHTML;
			el.node.innerHTML = svg;
			if(point) el.addTo(point,true);
		});

		el.click(function(event){
			event.stopPropagation();
			console.log(el);
		});

		el.addTo = function(point,direct){
			var that = this;
			if(!point) return false;
			var offsetY = 0;
			var actualItems = point.selectAll('.note');
			actualItems.forEach(function(item){
				if(item.getBBox().y > offsetY){
					offsetY = item.getBBox().y;
				}
			});
			if(actualItems.length > 0) offsetY+=5;
			//if adding from context-menu
			if(direct){
				point.gElements.notes.add(that);
				that.moveXY(0,offsetY);
				that.update();
			}else{
				that.animateXY(
					point.getBBox().x + point.gElements.notes.getBBox().x,
					point.getBBox().y + point.gElements.notes.getBBox().y + offsetY + 5,
					function(){
						point.gElements.notes.add(that);
						that.moveXY(0,offsetY);
						that.update();
					}
				);
			}
		};

		el.connect = function(){
			var that = this;
			//get closest points
			var x = that.getBBox().x + that.settings.center.x;
			var y = that.getBBox().y + that.settings.center.y;

			var points = [];
			gridApp.gPoints.selectAll('.point').forEach(function(el){
				points.push({id:el.node.id, x: el.getCenter().x, y: el.getCenter().y});
			});

			if(points.length > 0){
				var point = gridApp.getClosest({x:x, y:y},points);
				var gPoint = gridApp.gPoints.select('#'+point.id);
				if(gPoint) that.addTo(gPoint);
			}
		};

		el.mousedown(function(event){
			event.stopPropagation();
			if (event.which == 3){
				$('#point-element-menu').show().css({ top: event.clientY, left: event.clientX}).attr('data-id',el.attr('id'));
			}
		});

		el.update = function(){
			var that = this;
			angularScope.updateNote({id: that.idx}, function(result){
				if(result.success){	
					//that.synch();
				}else{
					that.remove();
				}
			});
			angularScope.$apply();
		}

		return el;

	};
});