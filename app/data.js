//initial data
var init = {
	points : [
		{
			id: 1, 
			start_date: 395829349, 
			end_date: 4352352462,
			place: {
				country: {id: 'UA', title: 'Украина'}, 
				city: {id: "ChIJBUVa4U7P1EAR_kYBF9IxSXY", title: 'Киев'}
			},
			accomodations: [
				{
					id: 1,
					....
				}
			],
			notes: [
				{
					id: 1,
					....
				}
			],
			activities: [
				{
					id: 1,
					....
				}
			]
		},
		...
	],
	transfers : [
		{
			id: 1,
			start_date: 352355325
			end_date: 2352525532,
			point_from: 1,
			point_to: 2,
			type: 'flight',
			notes: '....',
			...

		},
		...
	]
};



