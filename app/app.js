//drawing paper
var canvas = Snap('#canvas');

//main groups of elements (important positions!)
gridApp.gCountries = canvas.g().attr({id: 'grid-countries'});
gridApp.gDays = canvas.g().attr({id: 'grid-days'});
gridApp.gPanel = canvas.select('#panel');
gridApp.gConnections = canvas.g().attr({id: 'connections'});
gridApp.gPoints = canvas.g().attr({id: 'points'});


//initial data
var init = {
	points : [
		{
			id: 1, 
			time: 0, 
			place: {
				country: {id: 'UA', title: 'Украина'}, 
				city: {id: "ChIJBUVa4U7P1EAR_kYBF9IxSXY", title: 'Киев'}
			}
		},
		{
			id: 2, 
			time: 24*3600,
			place: {
				country: {id: 'HU', title: 'Венгрия'}, 
				city: {id: "ChIJyc_U0TTDQUcRYBEeDCnEAAQ", title: 'Будапешт'}
			}
		},
		{
			id: 3, 
			time: 3*24*3600,
			place: {
				country: {id: 'AT', title: 'Австрия'}, 
				city: {id: "ChIJn8o2UZ4HbUcRRluiUYrlwv0", title: 'Вена'}
			}
		}
	],
	transfers : [
		{

		},
		{
			
		}
	]
};


//add Days
canvas.Day(5); //start from saturday
//create 4 days more
for(var i = 0; i < 4; i++){
	canvas.Day();
}

//add Points
for(var i = 0; i < init.points.length; i++){
	var point = canvas.Point(init.points[i]);
	if(i > 0) point.draggable();
}


/***** CUSTOM CONTEXT MENU ******/
document.addEventListener('contextmenu', function(event) {
	event.preventDefault();
});

canvas.mousedown(function(event){
	event.stopPropagation();
	if (event.which == 3){
		$('#canvas-menu').show().css({top: event.clientY, left: event.clientX});
	}
});

$('#canvas-menu li').click(function(){
	var action = $(this).data('action');
	switch(action){
		case 'add-point':
			var point = canvas.Point();
			point.moveXY($('#canvas-menu').css('left').replace('px',''), $('#canvas-menu').css('top').replace('px',''));
			point.draggable();
			point.update();
			break;
		default:
			console.log('no action for this item');
	}
});

$('#point-menu li').click(function(){
	var id = $('#point-menu').attr('data-id');
	if(!id){
		return false;
	}

	var point = gridApp.gPoints.select('#'+id);
	var action = $(this).data('action');
	switch(action){
		case 'edit':
			point.update();
			break;
		case 'delete':
			point.delete();
			break;
		case 'transfer':
			var transfer = canvas.Transfer();
			transfer.moveXY(point.getBBox().x - 40, point.getBBox().y);
			transfer.connect();
			break;
		case 'accomodation':
			canvas.Accomodation(null,point);
			//accomodation.update();
			break;
		case 'activity':
			canvas.Activity(null,point);
			//activity.update();
			break;
		case 'note':
			canvas.Note(null,point);
			//note.update();
			break;
		default:
			console.log('no action for this item');
	}
});

$('#transfer-menu').on('click', 'li', function(){
	var id = $('#transfer-menu').attr('data-id');
	if(!id) return false;
	var transfer = gridApp.gConnections.select('#'+id);
	var action = $(this).data('action');
	switch(action){
		case 'edit':
			transfer.update();
			break;
		case 'delete':
			transfer.delete();
			break;
		default:
			console.log('no action for this item');
	}
});

$('#point-element-menu li').click(function(){
	var id = $('#point-element-menu').attr('data-id');
	if(!id){
		return false;
	}
	var element = canvas.select('#'+id);
	var action = $(this).data('action');
	switch(action){
		case 'edit':
			element.update();
			break;
		case 'delete':
			element.remove();
			break;
		default:
			console.log('no action for this item');
	}
});

document.addEventListener('click', function(e) {
	$('.context-menu').hide();
});
/***** CUSTOM CONTEXT MENU END ******/


/**** ADDING ELEMENTS FROM PANEL ****/
var controlDragging = false;
//create element
var tempElement = null;
$('#panel').on('mousedown', '.control', function(e){
	controlDragging = true;
	switch($(this).attr('data-action')){
		case 'addPoint':
			tempElement = canvas.Point();
			tempElement.moveXY(e.pageX-tempElement.settings.center.x, e.pageY-tempElement.settings.center.y);
			break;
		case 'addTransfer':
			tempElement = canvas.Transfer();
			tempElement.setType('flight');
			tempElement.moveXY(e.pageX-tempElement.settings.center.x, e.pageY-tempElement.settings.center.y);
			break;
		case 'addAccomodation':
			tempElement = canvas.Accomodation();
			tempElement.moveXY(e.pageX-tempElement.settings.center.x, e.pageY-tempElement.settings.center.y);
			break;
		case 'addActivity':
			tempElement = canvas.Activity();
			tempElement.moveXY(e.pageX-tempElement.settings.center.x, e.pageY-tempElement.settings.center.y);
			break;
		case 'addNote':
			tempElement = canvas.Note();
			tempElement.moveXY(e.pageX-tempElement.settings.center.x, e.pageY-tempElement.settings.center.y);
			break;
		default:
			console.log('no action set');
	}
});
//dragging and update element
$('#main').on('mousemove mouseup', function(e){
	if(controlDragging){
		tempElement.moveXY(e.pageX-tempElement.settings.center.x, e.pageY-tempElement.settings.center.y);
	}
	//stop dragging
	if(e.type == 'mouseup'){
		controlDragging = false;
		if(tempElement){
			switch(tempElement.attr('class')){
				case 'point':
					tempElement.update();
					tempElement.draggable();
					break;
				case 'transfer':
					tempElement.connect();
					break;
				case 'accomodation':
					tempElement.connect();
					break;
				case 'activity':
					tempElement.connect();
					break;
				case 'note':
					tempElement.connect();
					break;
				default:
					console.log('undefined element class');
			}
		}
		tempElement = null;
	}
});