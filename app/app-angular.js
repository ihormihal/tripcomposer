//config
var url = {
	transfer: {
		get: '/server/transfers.json'
	},
	accomodation: {
		get: '/server/accomodations.json'
	}
};

var myApp = angular.module('myApp',[]);

myApp.controller('myController', ['$scope','$http', function($scope,$http){

	$scope.transfers = {
		types: ['flight','bus','railway','car','ship']
	};

	$scope.picker = null;

	$scope.pointReset = function(){
		$scope.point = {
			success: false, 
			id: null, 
			place: {} 
		};
	};
	$scope.transferReset = function(){
		$scope.transfer = {
			success: false, 
			id: null, 
			from: { locked: false, time: null }, 
			to: { locked: false, time: null }, 
			type: null, 
			data: null,
			search: {
				reaults: true
			}
		};
	};

	$scope.accomodationReset = function(){
		$scope.accomodation = {
			success: false, 
			id: null, 
			place: {id: null}, 
			data: null,
			search: {
				reaults: true
			}
		};
	};
	$scope.activityReset = function(){
		$scope.activity = {success: false, id: null, place: {id: null}, data: null};
	};
	$scope.noteReset = function(){
		$scope.note = {success: false, id: null, place: {id: null}, data: null};
	};

	$scope.pointReset();
	$scope.transferReset();
	$scope.accomodationReset();
	$scope.activityReset();
	$scope.noteReset();

	$scope.click = function(event){
		console.log(event.currentTarget);
	}

	$scope.updatePoint = function(data, callback){
		console.log(data);
		$scope.point.id = data.id;
		if('city' in data.place){
			$scope.point.place = data.place;
		}
		$scope.setPoint = function(){
			if('city' in $scope.point.place && 'country' in $scope.point.place){
				$scope.point.success = true;
				callback($scope.point);
				$scope.pointReset();
			}else{
				$scope.point.success = false;
				//alert('Ошибка!');
				callback($scope.point);
				$scope.pointReset();
			}
		};
		$scope.popupPointClose = function(){
			if('city' in $scope.point.place && 'country' in $scope.point.place){
				$scope.point.success = true;
				callback($scope.point);
				$scope.pointReset();
			}else{
				$scope.point.success = false;
				//alert('Ошибка!');
				callback($scope.point);
				$scope.pointReset();
			}
		};
	};

	
	$scope.updateTransfer = function(data, callback){
		$scope.transfer.id = data.id;
		if(data.type){
			$scope.transfer.type = data.type;
		}
		if('city' in data.from.place){
			$scope.transfer.from.place = data.from.place;
			$scope.transfer.from.locked = true;
		}
		if('city' in data.to.place){
			$scope.transfer.to.place = data.to.place;
			$scope.transfer.to.locked = true;
		}
		if('time' in data.from){
			var now = (new Date).getTime();
			var time = data.from.time*1000 + now;

			var picker_ = $('.datepicker').pickadate({
				container: 'body',
            	selectMonths: true,
		    	selectYears: 2
            })
            .pickadate('picker')
            .set('select', time)
            .set('min', time)
            .on({
            	set: function(){
            		$scope.transfer.from.time = (this.get('select').pick - now)/1000;
            		//load new transfers
            		/*
            		$http.post(url.transfer.get,{
		    			//some data
		    		}).success(function(data, status, headers, config){
		    			//response here
		    			$scope.transfer.search.results = data.results;
		    		});
					*/
            	}
            });
		}

		$scope.searchTransfer = function(){
			$http.post(url.transfer.get,{
    			//some data
    		}).success(function(data, status, headers, config){
    			//response here
    			$scope.transfer.search.results = data.results;
    		});
		};
		$scope.setTransfer = function(){
			$scope.transfer.success = true;
			callback($scope.transfer);
			$scope.transferReset();
		};

		$scope.popupTransferClose = function(){
			if($scope.transfer.from && $scope.transfer.to && $scope.transfer.type){
				$scope.transfer.success = true;
				callback($scope.transfer);
			}else{
				$scope.transfer.success = false;
				$scope.transferReset();
				callback($scope.transfer);
			}
			$scope.transferReset();
		};
	};

	$scope.updateAccomodation = function(data, callback){
		$scope.accomodation.id = data.id;
		console.log(data);

		$scope.setAccomodation = function(){
			$scope.accomodation.success = true;
			callback($scope.accomodation);
			$scope.accomodationReset();
		};

		$scope.searchAccomodation = function(){
			$http.post(url.accomodation.get,{
    			//some data
    		}).success(function(data, status, headers, config){
    			//response here
    			$scope.accomodation.search.results = data.results;
    		});
		};

		$scope.popupAccomodationClose = function(){
			if($scope.accomodation.place.id){
				$scope.accomodation.success = true;
				callback($scope.accomodation);
			}else{
				$scope.accomodation.success = false;
				$scope.accomodationReset();
				callback($scope.accomodation);
			}
			$scope.accomodationReset();
		};
	};

	$scope.updateActivity = function(data, callback){
		$scope.activity.id = data.id;
		//console.log(data);

		$scope.setActivity = function(){
			$scope.activity.success = true;
			callback($scope.activity);
			$scope.activityReset();
		};
		$scope.popupActivityClose = function(){
			if($scope.activity.place.id){
				$scope.activity.success = true;
				callback($scope.activity);
			}else{
				$scope.activity.success = false;
				$scope.activityReset();
				callback($scope.activity);
			}
			$scope.activityReset();
		};
	};

	$scope.updateNote = function(data, callback){
		$scope.note.id = data.id;
		console.log(data);

		$scope.setNote = function(){
			$scope.note.success = true;
			callback($scope.note);
			$scope.noteReset();
		};
		$scope.popupNoteClose = function(){
			if($scope.note.place.id){
				$scope.note.success = true;
				callback($scope.note);
			}else{
				$scope.note.success = false;
				$scope.noteReset();
				callback($scope.note);
			}
			$scope.noteReset();
		};
	};

}]);


myApp.directive('googlecity', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, model) {
        	
        	model.$render = function() {};

            var options = {
                language: 'ru',
    			types: ['(cities)']
            };
            var gPlace = new google.maps.places.Autocomplete(element[0], options);

            google.maps.event.addListener(gPlace, 'place_changed', function() {
				var result = gPlace.getPlace();
				var place = {};
				for(var i=0; i < result.address_components.length; i++){
					//console.log(result);
			    	if(result.address_components[i].types.indexOf('locality') !== -1){
			    		place.city = {id: result.place_id, title: result.address_components[i].long_name, short_title: result.address_components[i].short_name};
			    	}
			    	if(result.address_components[i].types.indexOf('country') !== -1){
			    		place.country = {id: result.address_components[i].short_name, title: result.address_components[i].long_name};
			    	}
			    }
				scope.$apply(function() {
                    model.$setViewValue(place);
                    //console.log(model.$viewValue);
                });
			});
        }
    };
});

myApp.directive('googleplace', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, model) {
        	
        	model.$render = function() {};

            var options = {
            	//componentRestrictions: { country: "uk" },
                language: 'ru'
            };
            var gPlace = new google.maps.places.Autocomplete(element[0], options);

            google.maps.event.addListener(gPlace, 'place_changed', function() {
				var result = gPlace.getPlace();
				console.log(result);
				var place = {};
				for(var i=0; i < result.address_components.length; i++){
			    	if(result.address_components[i].types.indexOf('locality') !== -1){
			    		place.city = {id: result.place_id, title: result.address_components[i].long_name, short_title: result.address_components[i].short_name};
			    	}
			    	if(result.address_components[i].types.indexOf('country') !== -1){
			    		place.country = {id: result.address_components[i].short_name, title: result.address_components[i].long_name};
			    	}
			    }
				scope.$apply(function() {
                    model.$setViewValue(place);
                    //console.log(model.$viewValue);
                });
			});
        }
    };
});
/*
myApp.directive('pickadate', function() {
	var picker = null;
    return {
        restrict: 'A',
        link: function($scope, element, attrs) {
            angular.element(element).pickadate({
            	selectMonths: true,
		    	selectYears: 2
            });
        }
    }
});
*/